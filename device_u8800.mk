$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

# Proprietary stuff
$(call inherit-product-if-exists, vendor/huawei/u8800/u8800-vendor.mk)

# Use standard dalvik heap sizes
$(call inherit-product, frameworks/native/build/phone-hdpi-512-dalvik-heap.mk)

# Include keyboards
$(call inherit-product-if-exists, device/huawei/u8800/keyboards/keyboards.mk)

# Include scripts
$(call inherit-product-if-exists, device/huawei/u8800/initscripts/initscripts.mk)

DEVICE_PACKAGE_OVERLAYS += device/huawei/u8800/overlay

LOCAL_PATH := device/huawei/u8800

#ifeq ($(TARGET_PREBUILT_KERNEL),)
#	LOCAL_KERNEL := $(LOCAL_PATH)/kernel
#else
#	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
#endif

#PRODUCT_COPY_FILES += \
#    $(LOCAL_KERNEL):kernel

# U8800 uses high-density artwork where available
PRODUCT_AAPT_CONFIG := normal hdpi
PRODUCT_AAPT_PREF_CONFIG := hdpi

# Hardware-specific features
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml

# Init scripts
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.emmc.rc:root/init.emmc.rc \
	$(LOCAL_PATH)/ramdisk/init.qcom.rc:root/init.qcom.rc \
	$(LOCAL_PATH)/ramdisk/init.qcom.sh:root/init.qcom.sh \
	$(LOCAL_PATH)/ramdisk/init.qcom.usb.rc:root/init.qcom.usb.rc \
	$(LOCAL_PATH)/ramdisk/init.qcom.usb.sh:root/init.qcom.usb.sh \
	$(LOCAL_PATH)/ramdisk/init.rc:root/init.rc \
	$(LOCAL_PATH)/ramdisk/initlogo.rle:root/initlogo.rle \
	$(LOCAL_PATH)/ramdisk/ueventd.rc:root/ueventd.rc

# The fstab file
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/fstab.u8800:root/fstab.u8800 \
	$(LOCAL_PATH)/fstab.sdcard:root/fstab.sdcard \
	$(LOCAL_PATH)/vold.fstab:system/etc/vold.fstab

# Audio
PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/audio_policy.conf:system/etc/audio_policy.conf

# Allow emulated sdcard on /data/media
PRODUCT_PROPERTY_OVERRIDES += \
	persist.fuse_sdcard=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.sf.lcd_density=240

# Audio
PRODUCT_PROPERTY_OVERRIDES += \
	lpa.decode=true \
	use.non-omx.mp3.decoder=true

# Graphics
PRODUCT_PROPERTY_OVERRIDES += \
	debug.composition.type=gpu \
	debug.egl.hw=1 \
	debug.sf.hw=1

PRODUCT_PROPERTY_OVERRIDES += \
    ro.opengles.version=131072

$(call inherit-product, build/target/product/full.mk)

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0
PRODUCT_NAME := full_u8800
PRODUCT_DEVICE := u8800
PRODUCT_BRAND := Huawei
PRODUCT_MANUFACTURER := Huawei
PRODUCT_MODEL := u8800
